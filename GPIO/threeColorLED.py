import RPi.GPIO as GPIO
import time, sys

bChannel = 22
rChannel = 18
gChannel = 17

while True:
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(bChannel,GPIO.OUT)
    GPIO.setup(rChannel,GPIO.OUT)
    GPIO.setup(gChannel,GPIO.OUT)
    
    #violet
    GPIO.output(bChannel, GPIO.HIGH)
    GPIO.output(rChannel, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(bChannel, GPIO.LOW)
    GPIO.output(rChannel, GPIO.LOW)
    
    #blue
    GPIO.output(bChannel, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(bChannel, GPIO.LOW)
    
    #aqua
    GPIO.output(bChannel, GPIO.HIGH)
    GPIO.output(gChannel, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(bChannel, GPIO.LOW)
    GPIO.output(gChannel, GPIO.LOW)
    
    #green
    GPIO.output(gChannel, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(gChannel, GPIO.LOW)
    
    #yellow
    GPIO.output(rChannel, GPIO.HIGH)
    GPIO.output(gChannel, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(rChannel, GPIO.LOW)
    GPIO.output(gChannel, GPIO.LOW)
    
    #orange
    p1 = GPIO.PWM(gChannel, 100);
    p2 = GPIO.PWM(rChannel, 100);
    p1.start(33)
    p2.start(100)
    time.sleep(0.5)
    p1.stop()
    p2.stop()
    #red
    p2.start(100)
    time.sleep(0.5)
    p2.stop()