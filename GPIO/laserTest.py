import RPi.GPIO as GPIO, time
import numpy as np

# Tell the GPIO library to use
# Board GPIO references
GPIO.setmode(GPIO.BOARD)
GPIO.setup(10, GPIO.OUT)

# Define function to create pulses
def LaserLoop(timeOn):
    # Define laser output pin (3V3)
    GPIO.output(10, GPIO.HIGH)
    print(timeOn)
    time.sleep(float(timeOn))
    GPIO.output(10, GPIO.LOW)
    time.sleep(2)

# Main program loop
while True:
    print('So, you want to fire a laser! How long should it stay on (ms)?')
    a = raw_input()
    timeOn = np.true_divide(a, 1000)
    print(timeOn)
    LaserLoop(timeOn) # Crank that laser    