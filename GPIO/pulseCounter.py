import RPi.GPIO as GPIO, time

# Tell the GPIO library to use
# Board GPIO references
GPIO.setmode(GPIO.BCM)
measurement = 0
sTime = time.clock()
# Define function to measure charge time
def MeasureLoop(measurement):
    while True:
        GPIO.setup(17, GPIO.IN)
        # Count loops if voltage across
        # pin reads high on GPIO
        if (GPIO.input(17) == GPIO.HIGH):
            measurement = measurement+ 1
            pTime = time.clock()-sTime
            print ('Pulse: %d') %measurement
            print('Time: %4.4f') %pTime
            time.sleep(.75)

# Main program loop
while True:
    MeasureLoop(measurement)