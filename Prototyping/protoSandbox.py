#import time, numpy as np
#mport RPi.GPIO as GPIO

# Tell the GPIO library to use
# Board GPIO references
#GPIO.setmode(GPIO.BCM)

# Define function to measure charge time
#def MeasureLoop():
#    while True:
#        GPIO.setup(17, GPIO.IN)
#        input_value = GPIO.input(17)
#        print(input_value)

# Main program loop
#while True:
#  MeasureLoop() # Measure timing using pin 17
########


import time, signal, sys
# Point towards Adafruit ADC library folder then load it
sys.path.append("/usr/share/adafruit/webide/repositories/Adafruit-Raspberry-Pi-Python-Code/Adafruit_ADS1x15")
from Adafruit_ADS1x15 import ADS1x15

ADS1115 = 0x01
adc = ADS1x15(ic=ADS1115)

# Sample every 500ms and print voltage
while True:
    # channel A0, gain to +-4.096v, 250S/s
    volts = adc.readADCSingleEnded(0,4096, 250)/1000
    volts2 = adc.readADCSingleEnded(1,4096, 250)/1000
    print(volts)
    print('GND %d') %volts2
    time.sleep(0.5)