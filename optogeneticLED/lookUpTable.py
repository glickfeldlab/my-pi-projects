import time, sys, os
import numpy as np
import csv
from scipy import stats
from scipy.interpolate import interp1d

import pylab as pl
from pylab import plot, show


def computePower(power, lookUpTable):
    powerL = []
    voltageL = []
    # read the fil
    readerFile = csv.DictReader(open(lookUpTable, 'rb'), 
    fieldnames=('voltage', 'power'), 
    delimiter=' ', skipinitialspace=True)
    # parse into 
    for line in readerFile:
        if line['power'] is not None:
            powerL.append(float(line['power']))
            voltageL.append(float(line['voltage']))
            
    # convert to numpy arrays
    powerV = np.asarray(powerL, dtype='float64')
    voltageV = np.asarray(voltageL, dtype='float64')
    # actual interpolation done here
    f = interp1d(powerV,voltageV)
    power = float(power)
    voltage = f(power)
    # Plotting
    #pl.ion()
    #pH, = pl.plot(voltageV,powerV, 'b.-')# ,powerOutput,power)
    #pH2 = pl.plot(voltage, power, 'ro')
    #pl.xlabel('Command Voltage (V)')
    #pl.ylabel('Calculated LED Output (mW)')
    #pl.gca().autoscale_view(1,1,1)
    #pl.draw()
    #time.sleep(10)
    return voltage