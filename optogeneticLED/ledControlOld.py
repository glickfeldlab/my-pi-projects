import time, signal, sys, socket
import numpy as np
import RPi.GPIO as GPIO
# Point towards Adafruit DAC library folder then load it
sys.path.append("/usr/share/adafruit/webide/repositories/Adafruit-Raspberry-Pi-Python-Code/Adafruit_MCP4725")
from Adafruit_MCP4725 import MCP4725
from lookUpTable import computePower


# initialize control elements
dac = MCP4725(0x62)
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN)

#calculate voltage
volts = computePower(2.5)
maxVal = 4096
voltageSource = 5.0
voltsInBits = [int((volts/voltageSource)*maxVal)]
print(voltsInBits)
d=0


# main loop
while True:
    while (GPIO.input(17) == GPIO.HIGH):
        for val in voltsInBits:
            dac.setVoltage(val)
        if d==0:
            d=1
            s=0
    while (GPIO.input(17) == GPIO.LOW):
        dac.setVoltage(-1)
        d=0
        volts = computePower(2.5)
        maxVal = 4096
        voltageSource = 5.0
        voltsInBits = [int((volts/voltageSource)*maxVal)]
        #print(voltsInBits)