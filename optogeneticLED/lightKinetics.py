import time, signal, sys, socket
import numpy as np
import os
import pickle as p
import RPi.GPIO as GPIO
# Point towards Adafruit DAC library folder then load it
sys.path.append("/usr/share/adafruit/webide/repositories/Adafruit-Raspberry-Pi-Python-Code/Adafruit_MCP4725")
from Adafruit_MCP4725 import MCP4725
import threading
from threading import Thread
sys.path.append("/usr/share/adafruit/webide/repositories/Adafruit-Raspberry-Pi-Python-Code/Adafruit_ADS1x15")
from Adafruit_ADS1x15 import ADS1x15    
from lookUpTable import computePower

# Configure the DAC for output
dac = MCP4725(0x62)
dac.setVoltage(-1)
# Configure the ADS1115 to receive parameters from AO
ADS1115 = 0x01
adc = ADS1x15(ic=ADS1115)

requestedPowerMw = 20
rangeValue = 21 # max range of power meter (mW)
stopS = 30 #number of seconds of data to be acquired
respV = []


#calculate voltage
volts = computePower(requestedPowerMw, "/home/pi/Desktop/laser532nm.txt")
maxVal = 4096
voltageSource = 5.0
voltsInBits = [int((volts/voltageSource)*maxVal)]

# main loop
for val in voltsInBits:
    dac.setVoltage(val)
    
# loop and collect data
vLevels = np.linspace(1, stopS*2, stopS*2)
for i in vLevels:
    tVolts = adc.readADCSingleEnded(0,4096, 250)/1000
    tDataCol = (tVolts*rangeValue)/2
    print tDataCol   
    responseData = tDataCol
    #responseData = np.array([x for x in tDataCol])
    #load into a vector - discard pts at start for stability
    respV.append(responseData)
    time.sleep(.5)


print 'Press enter to write LUT and exit'
nodata=raw_input()
outName = os.path.expanduser('/home/pi/Desktop/franksKinetics20mw.txt')
print outName

fd = open(outName, 'w')
for (a) in zip(respV):
    fd.write('%7.4f\n' % (a))
fd.close()
dac.setVoltage(-1)